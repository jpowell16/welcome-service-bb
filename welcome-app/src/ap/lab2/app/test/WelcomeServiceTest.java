package ap.lab2.app.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ap.lab2.app.WelcomeService;

class WelcomeServiceTest {
	

	@Test
public  void testAdd() {
		WelcomeService service = new WelcomeService();
		
		String name = "Powell";
		String expectedResult = "Hello" + name;
		String actualResult = service.greet(name);
		
		assertEquals(expectedResult, actualResult);
	}

}
